<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

<title>Administratori de camin | Home</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body
	style="background-image: url(http://www.webdesignburn.com/wp-content/uploads/2015/04/Grey-Texture-Background-and-Wallpaper-16.jpg)">


	<div role="navigation">
		<button class="glyphicon glyphicon-home">
			<a href="home"> Home</a>
		</button>
		<div class="navbar navbar-inverse">
			<a href="/admin-campus" class="navbar-brand">Administratori de
				campus</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-admin-campus">Administrator de campus nou</a></li>
					<li><a href="all-admini-campus">Toti administratorii de
							campus</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">
				<div class="jumbotron text-center">
					<h1>Bine ai venit la pagina administratorilor de campus!</h1>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_ADMINI_CAMPUS'}">
			<div class="container text-center" id="admincaminsDiv">
				<h3>Administratori</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nume</th>
								<th>Prenume</th>
								<th>Numar telefon</th>

								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="admin_campus" items="${admini_campus}">
								<tr>
									<td>${admin_campus.id}</td>
									<td>${admin_campus.nume}</td>
									<td>${admin_campus.prenume}</td>
									<td>${admin_campus.nr_telefon}</td>

									<td><a href="update-admin-campus?id=${admin_campus.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-admin-campus?id=${admin_campus.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Administratori de campus</h3>
				<hr>
				<form class="form-horizontal" method="POST"
					action="save-admin-campus">
					<input type="hidden" name="id" value="${admin_campus.id}" />
					<div class="form-group">
						<label class="control-label col-md-3">Nume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nume"
								value="${admin_campus.nume}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Prenume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="prenume"
								value="${admin_campus.prenume}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Numar telefon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_telefon"
								value="${admin_campus.nr_telefon}" pattern="^\d{4}-\d{3}-\d{4}$"
								placeholder="XXXX-XXX-XXXX" required="required" />
						</div>
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>


</body>
</html>