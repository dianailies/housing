<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

<title>Administratori de camin | Home</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body
	style="background-image: url(http://www.webdesignburn.com/wp-content/uploads/2015/04/Grey-Texture-Background-and-Wallpaper-16.jpg)">


	<div role="navigation">
		<button class="glyphicon glyphicon-home">
			<a href="home"> Home</a>
		</button>
		<div class="navbar navbar-inverse">
			<a href="/admin-camin" class="navbar-brand">Administratori de
				camine</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-admin-camin">Administrator de camin nou</a></li>
					<li><a href="all-admini-camin">Toti administratorii de
							camin</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">
				<div class="jumbotron text-center">
					<h1>Bine ai venit la pagina administratorilor de camin!</h1>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_ADMINI_CAMIN'}">
			<div class="container text-center" id="admincaminsDiv">
				<h3>Administratori</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nume</th>
								<th>Prenume</th>
								<th>Numar telefon</th>

								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="admin_camin" items="${admini_camin}">
								<tr>
									<td>${admin_camin.id}</td>
									<td>${admin_camin.nume}</td>
									<td>${admin_camin.prenume}</td>
									<td>${admin_camin.nr_telefon}</td>

									<td><a href="update-admin-camin?id=${admin_camin.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-admin-camin?id=${admin_camin.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Administratori de camin</h3>
				<hr>
				<form class="form-horizontal" method="POST"
					action="save-admin-camin">
					<input type="hidden" name="id" value="${admin_camin.id}" />
					<div class="form-group">
						<label class="control-label col-md-3">Nume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nume"
								value="${admin_camin.nume}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Prenume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="prenume"
								value="${admin_camin.prenume}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Numar telefon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_telefon"
								value="${admin_camin.nr_telefon}" pattern="^\d{4}-\d{3}-\d{4}$"
								placeholder="XXXX-XXX-XXXX" required="required" />
						</div>
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>


</body>
</html>