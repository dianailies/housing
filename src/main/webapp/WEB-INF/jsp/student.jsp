<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

<title>Student | Home</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</head>
<body
	style="background-image: url(http://www.webdesignburn.com/wp-content/uploads/2015/04/Grey-Texture-Background-and-Wallpaper-16.jpg);">



	<div role="navigation">
		<button class="glyphicon glyphicon-home">
			<a href="home"> Home</a>
		</button>
		<div class="navbar navbar-inverse">

			<a href="/student" class="navbar-brand">Studenti</a>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-student">Student nou</a></li>
					<li><a href="all-students">Toti studentii</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">

				<div class="jumbotron text-center">
					<h1>Bine ai venit la pagina studentilor!</h1>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_STUDENTS'}">
			<div class="container text-center" id="studentiDiv">
				<h3>Studenti</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nume</th>
								<th>Prenume</th>
								<th>Username</th>
								<th>Parola</th>
								<th>Email</th>
								<th>Numar de telefon</th>
								<th>Adresa</th>
								<th>Data crearii contului</th>
								<th>Facultate</th>
								<th>Sex</th>



								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="student" items="${students}">
								<tr>
									<td>${student.id}</td>
									<td>${student.nume}</td>
									<td>${student.prenume}</td>
									<td>${student.username}</td>
									<td>${student.parola}</td>
									<td>${student.email}</td>
									<td>${student.nr_telefon}</td>
									<td>${student.adresa}</td>
									<td>${student.data_crearii}</td>
									<td>${student.facultate}</td>
									<td>${student.sex}</td>

									<td><a href="update-student?id=${student.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-student?id=${student.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Studenti</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-student">
					<input type="hidden" name="id" value="${student.id}" />


					<div class="form-group">
						<label class="control-label col-md-3">Nume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nume"
								value="${student.nume}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Prenume</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="prenume"
								value="${student.prenume}" required="required" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username"
								value="${student.username}" required="required" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Parola</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="parola"
								value="${student.parola}" required="required" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Email</label>
						<div class="col-md-7">
							<input type="email" class="form-control" name="email" placeholder="example@smth.com"
								value="${student.email}" required="required" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Numar de telefon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_telefon"
								pattern="^\d{4}-\d{3}-\d{4}$" placeholder="XXXX-XXX-XXXX" required
								value="${student.nr_telefon}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Adresa</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="adresa"
								value="${student.adresa}" required="required" />
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3">Facultate</label>
						<div class="col-md-7 dropdown">
							<input type="text" class="form-control" name="facultate"
								value="${student.facultate}" required="required" />

						</div>
					</div>




					<div class="form-group">
						<label class="control-label col-md-3">Sex</label>
						<div class="col-md-7">
							<input type="radio" class="col-sm-1" name="sex" value="Feminin" />
							<div class="col-sm-2">Feminin</div>
							<input type="radio" class="col-sm-1" name="sex" value="Masculin"
								checked />
							<div class="col-sm-2">Masculin</div>
						</div>
					</div>







					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>


</body>
</html>