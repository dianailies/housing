<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

<title>Camine | Home</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body
	
	style="background-image:url(http://www.webdesignburn.com/wp-content/uploads/2015/04/Grey-Texture-Background-and-Wallpaper-16.jpg)">


	<div role="navigation">
	<button class="glyphicon glyphicon-home"><a href="home"> Home</a></button>
		<div class="navbar navbar-inverse">
			<a href="/camin" class="navbar-brand">Camine</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-camin">Camin nou</a></li>
					<li><a href="all-camine">Toate caminele</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">
				<div class="jumbotron text-center">
					<h1>Bine ai venit la pagina caminelor!</h1>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_CAMINE'}">
			<div class="container text-center" id="caminsDiv">
				<h3>Camine</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Numar de camere</th>
								<th>Admin</th>


								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="camin" items="${camine}">
								<tr>
									<td>${camin.id}</td>
									<td>${camin.nr_camere}</td>
									<td>${camin.admin_camin}</td>


									<td><a href="update-camin?id=${camin.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-camin?id=${camin.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Camine</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-camin">
					<input type="hidden" name="id" value="${camin.id}" />
					<div class="form-group">
						<label class="control-label col-md-3">Numar de camere</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_camere"
								value="${camin.nr_camere}" required="required"/>
						</div>
					</div>




					<div class="form-group">
						<label class="control-label col-md-3">Admin Camin</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="admin_camin"
								value="${camin.admin_camin}" required="required"/>
						</div>
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>


</body>
</html>