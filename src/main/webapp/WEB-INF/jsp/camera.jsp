<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

<title>Camine | Home</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body
	
	style="background-image:url(http://www.webdesignburn.com/wp-content/uploads/2015/04/Grey-Texture-Background-and-Wallpaper-16.jpg)">



	<div role="navigation">
	<button class="glyphicon glyphicon-home"><a href="home"> Home</a></button>
		<div class="navbar navbar-inverse">
			<a href="/camera" class="navbar-brand">Camere</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-camera">Camera noua</a></li>
					<li><a href="all-camere">Toate camerele</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">
				<div class="jumbotron text-center">
					<h1>Bine ai venit la pagina camerelor!</h1>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_CAMERE'}">
			<div class="container text-center" id="camerasDiv">
				<h3>Camere</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Status</th>
								<th>Pret</th>
								<th>Numar locuri ocupate</th>
								<th>Numar locuri libere</th>
								<th>Camin</th>



								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="camera" items="${camere}">
								<tr>
									<td>${camera.id}</td>
									<td>${camera.status}</td>
									<td>${camera.pret}</td>
									<td>${camera.nr_locuri_ocupate}</td>
									<td>${camera.nr_locuri_libere}</td>
									<td>${camera.camin}</td>

									<td><a href="update-camera?id=${camera.id}"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-camera?id=${camera.id}"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Camine</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-camera">
					<input type="hidden" name="id" value="${camera.id}" />

					<div class="form-group">
						<label class="control-label col-md-3">Status</label>
						<div class="col-md-7">
							<input type="radio" class="col-sm-1" name="status" value="ocupata" />
							<div class="col-sm-1">Ocupata</div>
							<input type="radio" class="col-sm-1" name="status" value="libera"
								checked />
							<div class="col-sm-1">Libera</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Pret</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="pret"
								value="${camera.pret}" required="required"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Numar locuri ocupate</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_locuri_ocupate"
								value="${camera.nr_locuri_ocupate}" required="required" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Numar locuri libere</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="nr_locuri_libere"
								value="${camera.nr_locuri_libere}" required="required"/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Camin</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="camin"
								value="${camera.camin}" required="required"/>
						</div>
					</div>

					





					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>
			</div>
		</c:when>
	</c:choose>


</body>
</html>