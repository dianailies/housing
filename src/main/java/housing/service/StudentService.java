package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.StudentRepository;
import housing.model.Student;

@Service
@Transactional
public class StudentService {

	private final StudentRepository studentRepository;

	public StudentService(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	public List<Student> findAll() {
		List<Student> studenti = new ArrayList<>();
		for (Student s : studentRepository.findAll()) {
			studenti.add(s);
		}
		return studenti;
	}

	public Student findStudent(int id) {
		return studentRepository.findOne(id);
	}

	public void save(Student Student) {
		studentRepository.save(Student);
	}

	public void delete(int id) {
		studentRepository.delete(id);
	}

}
