package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.PlataRepository;
import housing.model.Plata;

@Service
@Transactional
public class PlataService {
	private final PlataRepository plataRepository;

	public PlataService(PlataRepository plataRepository) {
		this.plataRepository = plataRepository;
	}

	public List<Plata> findAll() {
		List<Plata> plati = new ArrayList<>();
		for (Plata p : plataRepository.findAll()) {
			plati.add(p);
		}
		return plati;
	}

	public Plata findPlata(int id) {
		return plataRepository.findOne(id);
	}

	public void save(Plata Plata) {
		plataRepository.save(Plata);
	}

	public void delete(int id) {
		plataRepository.delete(id);
	}

}
