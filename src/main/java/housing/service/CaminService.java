package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.CaminRepository;
import housing.model.Camin;

@Service
@Transactional
public class CaminService {
	private final CaminRepository caminRepository;

	public CaminService(CaminRepository caminRepository) {
		this.caminRepository = caminRepository;
	}

	public List<Camin> findAll() {
		List<Camin> camine = new ArrayList<>();
		for (Camin c : caminRepository.findAll()) {
			camine.add(c);
		}
		return camine;
	}

	public Camin findCamin(int id) {
		return caminRepository.findOne(id);
	}

	public void save(Camin Camin) {
		caminRepository.save(Camin);
	}

	public void delete(int id) {
		caminRepository.delete(id);
	}

}
