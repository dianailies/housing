package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.CameraRepository;
import housing.model.Camera;

@Service
@Transactional
public class CameraService {
	private final CameraRepository cameraRepository;

	public CameraService(CameraRepository cameraRepository) {
		this.cameraRepository = cameraRepository;
	}

	public List<Camera> findAll() {
		List<Camera> camere = new ArrayList<>();
		for (Camera l : cameraRepository.findAll()) {
			camere.add(l);
		}
		return camere;
	}

	public Camera findCamera(int id) {
		return cameraRepository.findOne(id);
	}

	public void save(Camera Camera) {
		cameraRepository.save(Camera);
	}

	public void delete(int id) {
		cameraRepository.delete(id);
	}

}
