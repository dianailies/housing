package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.ListaAsteptareRepository;
import housing.model.ListaAsteptare;

@Service
@Transactional
public class ListaAsteptareService {

	private final ListaAsteptareRepository listaRepository;

	public ListaAsteptareService(ListaAsteptareRepository listaRepository) {
		this.listaRepository = listaRepository;
	}

	public List<ListaAsteptare> findAll() {
		List<ListaAsteptare> liste = new ArrayList<>();
		for (ListaAsteptare l : listaRepository.findAll()) {
			liste.add(l);
		}
		return liste;
	}

	public ListaAsteptare findList(int id) {
		return listaRepository.findOne(id);
	}

	public void save(ListaAsteptare listaAsteptare) {
		listaRepository.save(listaAsteptare);
	}

	public void delete(int id) {
		listaRepository.delete(id);
	}

}
