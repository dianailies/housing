package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.AdminCampusRepository;
import housing.model.AdminCampus;

@Service
@Transactional
public class AdminCampusService {
	private final AdminCampusRepository adminCampusRepository;

	public AdminCampusService(AdminCampusRepository adminCampusRepository) {
		this.adminCampusRepository = adminCampusRepository;
	}

	public List<AdminCampus> findAll() {
		List<AdminCampus> admins = new ArrayList<>();
		for (AdminCampus a : adminCampusRepository.findAll()) {
			admins.add(a);
		}
		return admins;
	}

	public AdminCampus findAdminCampus(int id) {
		return adminCampusRepository.findOne(id);
	}

	public void save(AdminCampus AdminCampus) {
		adminCampusRepository.save(AdminCampus);
	}

	public void delete(int id) {
		adminCampusRepository.delete(id);
	}

}
