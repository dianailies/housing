package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.AdminCaminRepository;
import housing.model.AdminCamin;

@Service
@Transactional
public class AdminCaminService {
	private final AdminCaminRepository adminCaminRepository;

	public AdminCaminService(AdminCaminRepository adminCaminRepository) {
		this.adminCaminRepository = adminCaminRepository;
	}

	public List<AdminCamin> findAll() {
		List<AdminCamin> admins = new ArrayList<>();
		for (AdminCamin a : adminCaminRepository.findAll()) {
			admins.add(a);
		}
		return admins;
	}

	public AdminCamin findAdminCamin(int id) {
		return adminCaminRepository.findOne(id);
	}

	public void save(AdminCamin AdminCamin) {
		adminCaminRepository.save(AdminCamin);
	}

	public void delete(int id) {
		adminCaminRepository.delete(id);
	}

}
