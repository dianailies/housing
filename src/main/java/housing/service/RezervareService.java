package housing.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import housing.dao.RezervareRepository;
import housing.model.Rezervare;

@Service
@Transactional
public class RezervareService {

	private final RezervareRepository rezervareRepository;

	public RezervareService(RezervareRepository rezervareRepository) {
		this.rezervareRepository = rezervareRepository;
	}

	public List<Rezervare> findAll() {
		List<Rezervare> rezervari = new ArrayList<>();
		for (Rezervare r : rezervareRepository.findAll()) {
			rezervari.add(r);
		}
		return rezervari;
	}

	public Rezervare findRezervare(int id) {
		return rezervareRepository.findOne(id);
	}

	public void save(Rezervare Rezervare) {
		rezervareRepository.save(Rezervare);
	}

	public void delete(int id) {
		rezervareRepository.delete(id);
	}

}
