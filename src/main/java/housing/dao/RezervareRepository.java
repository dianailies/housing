package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.Rezervare;

public interface RezervareRepository extends CrudRepository<Rezervare, Integer>{

}
