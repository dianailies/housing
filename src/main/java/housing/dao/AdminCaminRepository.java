package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.AdminCamin;



public interface AdminCaminRepository extends CrudRepository<AdminCamin, Integer>{

}
