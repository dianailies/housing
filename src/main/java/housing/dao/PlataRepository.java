package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.Plata;

public interface PlataRepository extends CrudRepository<Plata, Integer>{

}
