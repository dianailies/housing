package housing.dao;

import org.springframework.data.repository.CrudRepository;


import housing.model.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>{

}
