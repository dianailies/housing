package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.Camera;



public interface CameraRepository extends CrudRepository<Camera, Integer>{

}
