package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.ListaAsteptare;

public interface ListaAsteptareRepository extends CrudRepository<ListaAsteptare, Integer> {

}
