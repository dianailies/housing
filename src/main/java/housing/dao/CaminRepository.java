package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.Camin;

public interface CaminRepository extends CrudRepository<Camin, Integer>{

}
