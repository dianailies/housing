package housing.dao;

import org.springframework.data.repository.CrudRepository;

import housing.model.AdminCampus;

public interface AdminCampusRepository extends CrudRepository<AdminCampus, Integer> {

}
