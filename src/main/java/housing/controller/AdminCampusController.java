package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.AdminCampus;
import housing.service.AdminCampusService;

@Controller
public class AdminCampusController {

	@Autowired
	private AdminCampusService admin_campusService;

	

	@GetMapping("/admin-campus")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "adminCampus";
	}

	@GetMapping("/all-admini-campus")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("admini_campus", admin_campusService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMPUS");
		return "adminCampus";
	}

	@GetMapping("/new-admin-campus")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "adminCampus";
	}

	@PostMapping("/save-admin-campus")
	public String saveadmin_campus(@ModelAttribute AdminCampus AdminCampus, BindingResult bindingResult,
			HttpServletRequest request) {
		
		admin_campusService.save(AdminCampus);
		request.setAttribute("admini_campus", admin_campusService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMPUS");
		return "adminCampus";
	}

	@GetMapping("/update-admin-campus")
	public String updateadmin_campus(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("admin_campus", admin_campusService.findAdminCampus(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "adminCampus";
	}

	@GetMapping("/delete-admin-campus")
	public String deleteadmin_campus(@RequestParam int id, HttpServletRequest request) {
		admin_campusService.delete(id);
		request.setAttribute("admini_campus", admin_campusService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMPUS");
		return "adminCampus";
	}
}
