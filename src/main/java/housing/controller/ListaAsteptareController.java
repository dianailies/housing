package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.ListaAsteptare;
import housing.service.ListaAsteptareService;

@Controller
public class ListaAsteptareController {

	@Autowired
	private ListaAsteptareService listaService;

	

	@GetMapping("/list")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "listaAsteptare";
	}

	@GetMapping("/all-lists")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("lists", listaService.findAll());
		request.setAttribute("mode", "MODE_LISTS");
		return "listaAsteptare";
	}

	@GetMapping("/new-list")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "listaAsteptare";
	}

	@PostMapping("/save-list")
	public String saveList(@ModelAttribute ListaAsteptare listaAsteptare, BindingResult bindingResult,
			HttpServletRequest request) {
		
		listaService.save(listaAsteptare);
		request.setAttribute("lists", listaService.findAll());
		request.setAttribute("mode", "MODE_LISTS");
		return "listaAsteptare";
	}

	@GetMapping("/update-list")
	public String updateList(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("list", listaService.findList(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "listaAsteptare";
	}

	@GetMapping("/delete-list")
	public String deleteList(@RequestParam int id, HttpServletRequest request) {
		listaService.delete(id);
		request.setAttribute("lists", listaService.findAll());
		request.setAttribute("mode", "MODE_LISTS");
		return "listaAsteptare";
	}
}
