package housing.controller;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.Student;
import housing.service.StudentService;

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;

	
	@GetMapping("/student")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "student";
	}

	@GetMapping("/all-students")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("students", studentService.findAll());
		request.setAttribute("mode", "MODE_STUDENTS");
		return "student";
	}

	@GetMapping("/new-student")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "student";
	}

	@PostMapping("/save-student")
	public String savestudent(@ModelAttribute Student student, BindingResult bindingResult,
			HttpServletRequest request) throws ParseException {
		
		student.setData_crearii(new Date());
		studentService.save(student);
		request.setAttribute("students", studentService.findAll());
		request.setAttribute("mode", "MODE_STUDENTS");
		return "student";
	}

	@GetMapping("/update-student")
	public String updatestudent(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("student", studentService.findStudent(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "student";
	}

	@GetMapping("/delete-student")
	public String deletestudent(@RequestParam int id, HttpServletRequest request) {
		studentService.delete(id);
		request.setAttribute("students", studentService.findAll());
		request.setAttribute("mode", "MODE_STUDENTS");
		return "student";
	}
}
