package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.Camera;
import housing.service.CameraService;

@Controller
public class CameraController {

	@Autowired
	private CameraService cameraService;


	@GetMapping("/camera")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "camera";
	}

	@GetMapping("/all-camere")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("camere", cameraService.findAll());
		request.setAttribute("mode", "MODE_CAMERE");
		return "camera";
	}

	@GetMapping("/new-camera")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "camera";
	}

	@PostMapping("/save-camera")
	public String savecamera(@ModelAttribute Camera Camera, BindingResult bindingResult,
			HttpServletRequest request) {
		
		cameraService.save(Camera);
		request.setAttribute("camere", cameraService.findAll());
		request.setAttribute("mode", "MODE_CAMERE");
		return "camera";
	}

	@GetMapping("/update-camera")
	public String updatecamera(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("camera", cameraService.findCamera(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "camera";
	}

	@GetMapping("/delete-camera")
	public String deletecamera(@RequestParam int id, HttpServletRequest request) {
		cameraService.delete(id);
		request.setAttribute("camere", cameraService.findAll());
		request.setAttribute("mode", "MODE_CAMERE");
		return "camera";
	}
}
