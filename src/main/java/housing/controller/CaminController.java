package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.Camin;
import housing.service.CaminService;

@Controller
public class CaminController {

	@Autowired
	private CaminService caminService;

	

	@GetMapping("/camin")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "camin";
	}

	@GetMapping("/all-camine")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("camine", caminService.findAll());
		request.setAttribute("mode", "MODE_CAMINE");
		return "camin";
	}

	@GetMapping("/new-camin")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("camine", caminService.findAll());
		request.setAttribute("mode", "MODE_NEW");
		return "camin";
	}

	@PostMapping("/save-camin")
	public String savecamin(@ModelAttribute Camin Camin, BindingResult bindingResult,
			HttpServletRequest request) {
		
		caminService.save(Camin);
		request.setAttribute("camine", caminService.findAll());
		request.setAttribute("mode", "MODE_CAMINE");
		return "camin";
	}

	@GetMapping("/update-camin")
	public String updatecamin(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("camine", caminService.findAll());
		request.setAttribute("camin", caminService.findCamin(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "camin";
	}

	@GetMapping("/delete-camin")
	public String deletecamin(@RequestParam int id, HttpServletRequest request) {
		caminService.delete(id);
		request.setAttribute("camine", caminService.findAll());
		request.setAttribute("mode", "MODE_CAMINE");
		return "camin";
	}
}
