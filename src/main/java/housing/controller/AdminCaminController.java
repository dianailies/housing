package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.AdminCamin;
import housing.service.AdminCaminService;

@Controller
public class AdminCaminController {

	@Autowired
	private AdminCaminService admin_caminService;


	@GetMapping("/admin-camin")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "adminCamin";
	}

	@GetMapping("/all-admini-camin")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("admini_camin", admin_caminService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMIN");
		return "adminCamin";
	}

	@GetMapping("/new-admin-camin")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "adminCamin";
	}

	@PostMapping("/save-admin-camin")
	public String saveadmin_camin(@ModelAttribute AdminCamin adminCamin, BindingResult bindingResult,
			HttpServletRequest request) {
		
		admin_caminService.save(adminCamin);
		request.setAttribute("admini_camin", admin_caminService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMIN");
		return "adminCamin";
	}

	@GetMapping("/update-admin-camin")
	public String updateadmin_camin(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("admin_camin", admin_caminService.findAdminCamin(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "adminCamin";
	}

	@GetMapping("/delete-admin-camin")
	public String deleteadmin_camin(@RequestParam int id, HttpServletRequest request) {
		admin_caminService.delete(id);
		request.setAttribute("admini_camin", admin_caminService.findAll());
		request.setAttribute("mode", "MODE_ADMINI_CAMIN");
		return "adminCamin";
	}
}
