package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import housing.model.Plata;
import housing.service.PlataService;

@Controller
public class PlataController {

	@Autowired
	private PlataService plataService;

	

	@GetMapping("/plata")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "plata";
	}

	@GetMapping("/all-plati")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("plati", plataService.findAll());
		request.setAttribute("mode", "MODE_PLATI");
		return "plata";
	}

	@GetMapping("/new-plata")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "plata";
	}

	@PostMapping("/save-plata")
	public String saveplata(@ModelAttribute Plata Plata, BindingResult bindingResult, HttpServletRequest request) {

		plataService.save(Plata);
		request.setAttribute("plati", plataService.findAll());
		request.setAttribute("mode", "MODE_PLATI");
		return "plata";
	}

	@GetMapping("/update-plata")
	public String updateplata(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("plata", plataService.findPlata(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "plata";
	}

	@GetMapping("/delete-plata")
	public String deleteplata(@RequestParam int id, HttpServletRequest request) {
		plataService.delete(id);
		request.setAttribute("plati", plataService.findAll());
		request.setAttribute("mode", "MODE_PLATI");
		return "plata";
	}
}
