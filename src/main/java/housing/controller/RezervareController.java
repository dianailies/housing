package housing.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import housing.model.Rezervare;
import housing.service.RezervareService;

@Controller
public class RezervareController {

	@Autowired
	private RezervareService rezervareService;

	

	@GetMapping("/rezervare")
	public String home(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_HOME");
		return "rezervare";
	}

	@GetMapping("/all-rezervari")
	public String allTasks(HttpServletRequest request) {
		request.setAttribute("rezervari", rezervareService.findAll());
		request.setAttribute("mode", "MODE_REZERVARI");
		return "rezervare";
	}

	@GetMapping("/new-rezervare")
	public String newTask(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_NEW");
		return "rezervare";
	}

	@PostMapping("/save-rezervare")
	public String saverezervare(@ModelAttribute Rezervare Rezervare, BindingResult bindingResult,
			HttpServletRequest request) {
		
		rezervareService.save(Rezervare);
		request.setAttribute("rezervari", rezervareService.findAll());
		request.setAttribute("mode", "MODE_REZERVARI");
		return "rezervare";
	}

	@GetMapping("/update-rezervare")
	public String updaterezervare(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("rezervare", rezervareService.findRezervare(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "rezervare";
	}

	@GetMapping("/delete-rezervare")
	public String deleterezervare(@RequestParam int id, HttpServletRequest request) {
		rezervareService.delete(id);
		request.setAttribute("rezervari", rezervareService.findAll());
		request.setAttribute("mode", "MODE_REZERVARI");
		return "rezervare";
	}
}
