package housing.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin_campus")

public class AdminCampus implements Serializable{

	@Id
	@Column(name = "id_admin_campus")
	private int id;

	@Column(name = "nume")
	private String nume;

	@Column(name = "prenume")
	private String prenume;

	@Column(name = "nr_telefon")
	private String nr_telefon;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getNr_telefon() {
		return nr_telefon;
	}

	public void setNr_telefon(String nr_telefon) {
		this.nr_telefon = nr_telefon;
	}

	@Override
	public String toString() {
		return "AdminCampus [id=" + id + ", nume=" + nume + ", prenume=" + prenume + ", nr_telefon=" + nr_telefon + "]";
	}

}
