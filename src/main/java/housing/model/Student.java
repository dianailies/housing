package housing.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "student")

public class Student implements Serializable {

	@Id
	@Column(name = "id_student")
	private int id;

	@Column(name = "nume")
	private String nume;

	@Column(name = "prenume")
	private String prenume;

	@Column(name = "username")
	private String username;

	@Column(name = "parola")
	private String parola;

	@Column(name = "email")
	private String email;

	@Column(name = "nr_telefon")
	private String nr_telefon;

	@Column(name = "adresa")
	private String adresa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_crearii")
	private Date data_crearii;

	@Column(name = "facultate")
	private String facultate;

	@Column(name = "sex")
	private String sex;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getParola() {
		return parola;
	}

	public void setParola(String parola) {
		this.parola = parola;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNr_telefon() {
		return nr_telefon;
	}

	public void setNr_telefon(String nr_telefon) {
		this.nr_telefon = nr_telefon;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Date getData_crearii() throws ParseException {
		return data_crearii;
	}

	public void setData_crearii(Date date) throws ParseException {

		this.data_crearii = date;
	}

	public String getFacultate() {
		return facultate;
	}

	public void setFacultate(String facultate) {
		this.facultate = facultate;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", nume=" + nume + ", prenume=" + prenume + ", username=" + username + ", parola="
				+ parola + ", email=" + email + ", nr_telefon=" + nr_telefon + ", adresa=" + adresa + ", data_creare="
				+ data_crearii + ", facultate=" + facultate + ", sex=" + sex + "]";
	}

}
