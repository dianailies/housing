package housing.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lista_asteptare")
public class ListaAsteptare implements Serializable {

	public ListaAsteptare(int nr_studenti) {
		this.nr_studenti = nr_studenti;
	}
	public ListaAsteptare() {
		
	}

	@Id
	@Column(name = "id_lista_asteptare")
	private int id;

	@Column(name = "nr_studenti")
	private int nr_studenti;

	@Override
	public String toString() {
		return "ListaAsteptare [id=" + id + ", nr_studenti=" + nr_studenti + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNr_studenti() {
		return nr_studenti;
	}
	public void setNr_studenti(int nr_studenti) {
		this.nr_studenti = nr_studenti;
	}
}
