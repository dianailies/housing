package housing.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Camin")
public class Camin implements Serializable {
	@Id
	@Column(name = "id_camin")
	private int id;

	@Column(name = "nr_camere")
	private int nr_camere;

	@OneToOne
	@JoinColumn(name = "admin_camin_id_admin_camin")
	private AdminCamin admin_camin;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNr_camere() {
		return nr_camere;
	}

	public void setNr_camere(int nr_camere) {
		this.nr_camere = nr_camere;
	}

	public AdminCamin getAdmin_camin() {
		return admin_camin;
	}

	public void setAdmin_camin(AdminCamin admin_camin) {
		this.admin_camin = admin_camin;
	}

	@Override
	public String toString() {
		return "Camin [id=" + id + ", nr_camere=" + nr_camere + ", admin_camin=" + admin_camin + "]";
	}
}
