package housing.model;


import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Plata")
public class Plata implements Serializable{


	@Id
	@Column(name = "id_plata")
	private int id;
	
	@Column(name="suma")
	private int suma;
	
	@Column (name="data")
	private Date data;
	
	
	@ManyToOne
	@JoinColumn(name = "Student_id_student")
	private Student student;
	
	@ManyToOne
	@JoinColumn(name = "Student_Rezervare_id_rezervare")
	private Rezervare rezervare;
	
	@ManyToOne
	@JoinColumn(name = "AdminCampus_id_admin_campus")
	private AdminCampus adminCampus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSuma() {
		return suma;
	}

	public void setSuma(int suma) {
		this.suma = suma;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Rezervare getRezervare() {
		return rezervare;
	}

	public void setRezervare(Rezervare rezervare) {
		this.rezervare = rezervare;
	}

	public AdminCampus getAdminCampus() {
		return adminCampus;
	}

	public void setAdminCampus(AdminCampus adminCampus) {
		this.adminCampus = adminCampus;
	}

	@Override
	public String toString() {
		return "Plata [id=" + id + ", suma=" + suma + ", data=" + data + ", student=" + student + ", rezervare="
				+ rezervare + ", adminCampus=" + adminCampus + "]";
	}

	
}
