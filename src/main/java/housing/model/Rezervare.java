package housing.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Rezervare")
public class Rezervare implements Serializable{

	public Rezervare() {

	}

	public Rezervare(int id, Date data, Student student) {
		super();
		this.id = id;
		this.data = data;
	}

	@Id
	@Column(name = "id_rezervare")
	private int id;

	@Column(name = "data")
	private Date data;

	@ManyToOne
	@JoinColumn(name = "Camera_id_camera")
	private Camera camera;

	@ManyToOne
	@JoinColumn(name = "Camera_camin_id_camin")
	private Camin camin;

	@ManyToOne
	@JoinColumn(name = "lista_asteptare_id_lista_asteptare")
	private ListaAsteptare lista_asteptare;
	
	
	@OneToOne
	@JoinColumn(name = "student_id_student")
	private Student student;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public Camin getCamin() {
		return camin;
	}

	public void setCamin(Camin camin) {
		this.camin = camin;
	}

	public ListaAsteptare getLista_asteptare() {
		return lista_asteptare;
	}

	public void setLista_asteptare(ListaAsteptare lista_asteptare) {
		this.lista_asteptare = lista_asteptare;
	}

	@Override
	public String toString() {
		return "Rezervare [id=" + id + ", data=" + data + ", camera=" + camera + ", camin=" + camin
				+ ", lista_asteptare=" + lista_asteptare + "]";
	}

}
