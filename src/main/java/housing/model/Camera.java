package housing.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "camera")
public class Camera implements Serializable{

	@Id
	@Column(name = "id_camera")
	private int id;

	@Column(name = "status")
	private String status;

	@Column(name = "pret")
	private int pret;

	@Column(name = "nr_locuri_ocupate")
	private String nr_locuri_ocupate;

	@Column(name = "nr_locuri_libere")
	private String nr_locuri_libere;

	@OneToMany(mappedBy = "camera")
	private Set<Rezervare> camere;

	@ManyToOne
	@JoinColumn(name = "camin_id_camin")
	private Camin camin;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public Set<Rezervare> getCamere() {
		return camere;
	}

	public void setCamere(Set<Rezervare> camere) {
		this.camere = camere;
	}

	public Camin getCamin() {
		return camin;
	}

	public void setCamin(Camin camin) {
		this.camin = camin;
	}

	public String getNr_locuri_ocupate() {
		return nr_locuri_ocupate;
	}

	public void setNr_locuri_ocupate(String nr_locuri_ocupate) {
		this.nr_locuri_ocupate = nr_locuri_ocupate;
	}

	public String getNr_locuri_libere() {
		return nr_locuri_libere;
	}

	public void setNr_locuri_libere(String nr_locuri_libere) {
		this.nr_locuri_libere = nr_locuri_libere;
	}

	@Override
	public String toString() {
		return "Camera [id=" + id + ", status=" + status + ", pret=" + pret + ", nr_locuri_ocupate=" + nr_locuri_ocupate
				+ ", nr_locuri_libere=" + nr_locuri_libere + ", camere=" + camere + ", camin=" + camin + "]";
	}

}
